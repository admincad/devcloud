#!/bin/bash
set -euo pipefail


SPACK_REPO=https://github.com/spack/spack.git
SPACK_TAG=releases/v0.20
#SPACK_TAG=e4s-23.05


export SPACK_USER_CONFIG_PATH=/var/empty

#Install sapack
cd ..
git clone "${SPACK_REPO}" spack || true
cd spack
git remote set-url origin "${SPACK_REPO}"
git fetch --all
git reset --hard "origin/${SPACK_TAG}"
patch -p1 < ../build/gromacs_sycl.patch
source share/spack/setup-env.sh
cd ..

echo Building apps
cd build
spack compiler find --scope=site
spack env activate .
#export TMPDIR=$PBS_SCRATCHDIR
spack concretize -f
spack install --fresh -v -y
spack module tcl refresh -y
spack env deactivate
