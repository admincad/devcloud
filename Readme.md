# Devcloud

Esto genera un entorno independiente con spack ignorando las cofiguraciones que tengamos en nuestro home

# Uso
```
ssh devcloud
git clone <este proyecto>
cd devcloud/build
qsub submit.pbs # para mandar a compilar
```
Cuando eso terminó tenemos:
- build: para agregar al entorno preferencias de paquetes en  `packages.yaml` y concretos en `spack.yaml`
- modules: los modulos con los path a las apps concretas
- run: caso ejemplo de gromacs para mandar a ejecutar (cd run/caso_gpu && qsub submit.pbs)
- software: todo lo que se compiló con spack
- spack: la instalación de spack
